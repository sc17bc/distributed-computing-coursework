# Distributed Computing Coursework

A web services client that integrates multiple web services. The first service checks the weather of a place given by the user via the openweathermap API, then the second suggests how to prepare for said weather. Uses REST.
