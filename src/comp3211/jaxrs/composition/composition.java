package comp3211.jaxrs.composition;

import java.io.IOException;

import comp3211.jaxrs.recommendation.RecommendationRESTStart;
import comp3211.jaxrs.recommendation.client.RecommendationRESTClient;
import comp3211.jaxrs.weather.WeatherRESTClient;

public class composition {
	public static void main(String[] args) throws IOException {
		long startTime = System.currentTimeMillis();
		if (WeatherRESTClient.main(args) == 0) {
			System.out.println("Shutting down program");
			return;
		}
		long ellapsedTime = System.currentTimeMillis() - startTime;
		System.out.println("\nWeather data fetched and parsed in " + ellapsedTime + " milliseconds\n");
		RecommendationRESTStart.main(args);
		System.out.println();
		startTime = System.currentTimeMillis();
		RecommendationRESTClient.main(args);
		ellapsedTime = System.currentTimeMillis() - startTime;
		System.out.println("\nRecommendation fetched in " + ellapsedTime + " milliseconds\n");
	}
}
