package comp3211.jaxrs.weather;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Scanner;

import javax.json.Json;
import javax.json.stream.*;
import javax.json.stream.JsonParser.Event;

public class WeatherRESTClient {
	public static String getWeather() {
		String unparsedWeather = "";
		try { 
			BufferedReader fileReader = new BufferedReader(new FileReader("./weather.json")); //Reading from the generated JSON
			String jsonLine = "";
			try {
				while ((jsonLine = fileReader.readLine()) != null) {
					unparsedWeather += jsonLine;
				}
				fileReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			System.out.println("The weather file doesn't exist, this is likely because the city you entered wasn't in the openweathermap database");
			return null;
		}
		return unparsedWeather;
	}
	
	public static String parseWeather(String unparsedWeather) {
		JsonParser parser = Json.createParser(new StringReader(unparsedWeather));
		Event event = parser.next();
		String parsedWeather = null;
		while (parser.hasNext()) { //parsing the weather JSON
			event = parser.next();
			if (event == JsonParser.Event.valueOf("START_OBJECT")) {
				while ((event = parser.next()) != JsonParser.Event.valueOf("END_OBJECT")) {
					if (event == JsonParser.Event.valueOf("KEY_NAME")) {
						parsedWeather += "\n" + parser.getString() + ":";
					} else {
						parsedWeather += parser.getString();
					}
				}
			}
		}
		return parsedWeather;
	}

	public static int main(String[] args) throws FileNotFoundException {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the name of the city you'd like to see the weather for:");
		String city = input.nextLine();
		input.close();
		
		WeatherREST.getWeather(city); //Getting the weather info for the city the user enters
		if (getWeather() != null) {
			System.out.println(parseWeather(getWeather()));
			return 1;
		} else {
			return 0;
		}
	}

}
