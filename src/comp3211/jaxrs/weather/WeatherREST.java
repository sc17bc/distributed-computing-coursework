package comp3211.jaxrs.weather;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;

public class WeatherREST {
	static final String APIKey = "bdbdf5ef9ded84781809ee32f04e3296"; //API key for openweathermap
	
	public static void getWeather(String city) {
		URL url;
		HttpURLConnection connection = null;
		InputStream is = null;
		File file = new File("./weather.json");
		FileWriter fr = null;
		String reply = null;
		
		file.delete(); //Clearing file before use
		
		try {
			String urlString = "https://api.openweathermap.org/data/2.5/weather?q="
					+ city
					+ "&APPID="
					+ APIKey; //URL that returns the weather for the city the user input in string form
			
			url = new URL (urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect(); //connecting to said URL
            is = connection.getInputStream();
            BufferedReader weatherReader =
            new BufferedReader(new InputStreamReader(is, "UTF-8"));
            fr = new FileWriter(file);
            	try {
            		reply = weatherReader.readLine(); //writing the site's output to a JSON file
            		fr.write(reply);
            	} catch (IOException e) {
            		e.printStackTrace();
            	} finally {
            		try {
            			fr.close();
            		} catch (IOException e){
            			e.printStackTrace();
            		}
            	}
		} catch (FileNotFoundException e) {
			System.out.println("City isn't in the openweathermap database");
		} catch (Exception e) {
            e.printStackTrace();
		}
	}
}
