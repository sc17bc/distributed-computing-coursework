package comp3211.jaxrs.recommendation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/recommendation")
public class RecommendationREST { //path format is (weather id/temperature/wind speed)
	@GET
    @Path("/{id}/{temp}/{speed}")
	@Produces(MediaType.TEXT_PLAIN)
	public String recommendationPlaintext (@PathParam("id") Integer id, @PathParam("temp") double temp, @PathParam("speed") double speed) {
		String recommendation = ""; //Placeholder way of giving recommendation, there's probably a better way to do this than with if statements
		if (id == 1) { //Thunderstorm
			recommendation += "There's a thunderstorm going on, wear a coat if you're going out.\n";
		} else if (id == 2) { //Drizzle
			recommendation += "There's a drizzle, it might be worth bringing an coat.\n";
		} else if (id == 3) { //Rain
			recommendation += "It's raining, bring a coat or an umbrella.\n";
		} else if (id == 4) { //Snow
			recommendation += "It's snowing, bring a coat and maybe some gloves.\n";
		} else if (id == 5) {
			recommendation += "It's foggy, be careful if you're driving.\n";
		} else if (id == 6) {
			recommendation += "It's sunny, wear sunscreen.\n";
		} else if (id == 7) {
			recommendation += "It's cloudy, bring a coat in case it rains.\n";
		} 
		//temp is in kelvin
		if (temp <= 273) {
			recommendation += "It's below freezing, layer up.\n";
		} else if (temp > 273 && temp <= 283) {
			recommendation += "It's a bit chilly, maybe bring a jacket.\n";
		} else if (temp > 283) {
			recommendation += "It's warm, make sure to stay hydrated.\n";
		}
		
		if (speed > 1.6 && speed <= 3.4) {
			recommendation += "It's a little bit windy.\n";
		} else if (speed > 3.4 && speed <= 10.7) {
			recommendation += "It's fairly windy, it might be worth bringing a coat.\n";
		} else if (speed > 10.7 && speed <= 20.7) {
			recommendation += "It's very windy, bring a coat.\n";
		} else if (speed > 20.7) {
			recommendation += "It's windy enough that there might be flying debris, don't leave the house unless you have to.\n";
		}
		return recommendation;
	}
}
