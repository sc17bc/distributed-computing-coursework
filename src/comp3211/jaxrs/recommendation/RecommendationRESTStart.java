package comp3211.jaxrs.recommendation;

import java.io.IOException;
import java.net.URI;

import javax.ws.rs.ProcessingException;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;

public class RecommendationRESTStart {
	public static final String BASE_URI = "http://localhost:8080/";
	
	static HttpServer startServer() {
       	final ResourceConfig rc = new ResourceConfig().packages("comp3211.jaxrs.recommendation");
       	HttpServer server = null;
       	Boolean started = false;
       	try {
           	server = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
           	started = true;
       	} catch (ProcessingException e) {
       		System.out.println("The address is already in use, this is likely because the server has already been launched");
       	}
       	if (started) {
       		return server;
       	} else {
       		return null;
       	}
    }
		
	public static void main(String[] args) throws IOException {
       	final HttpServer server = startServer();
       	if (server != null) {
       		if(!server.isStarted()) {
           		server.start();
           	}
       		System.out.println("Server is running, press enter to stop it");
       		System.in.read();
           	server.shutdownNow();
       	}
    }
}