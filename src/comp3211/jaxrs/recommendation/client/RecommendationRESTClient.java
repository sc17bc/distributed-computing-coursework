package comp3211.jaxrs.recommendation.client;

import java.io.StringReader;

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import comp3211.jaxrs.weather.*;

public class RecommendationRESTClient {
	static final String REST_URI = "http://localhost:8080/recommendation";
	
	private static String getPath () {
		String unparsedWeather = WeatherRESTClient.getWeather();
		JsonParser parser = Json.createParser(new StringReader(unparsedWeather));
		Event event = parser.next();
		String path = "";
		Integer id = 0;
		Boolean idReached = false;
		while (parser.hasNext()) { //parsing the weather JSON
			event = parser.next();
			if (event == JsonParser.Event.valueOf("START_OBJECT")) {
				while ((event = parser.next()) != JsonParser.Event.valueOf("END_OBJECT")) {
					if (event == JsonParser.Event.valueOf("KEY_NAME")) {
						if (parser.getString().equals("id") && !idReached) {
							idReached = true;
							parser.next();
							id = parser.getInt(); //Placeholder code, probably a better way to do this than with if statements
							if (id < 300) { //Thunderstorm
								path += "/1";
							} else if (id >= 300 && id < 500) { //Drizzle
								path += "/2";
							} else if (id >= 500 && id < 600) { //Rain
								path += "/3";
							} else if (id >= 600 && id < 700) { //Snow
								path += "/4";
							} else if (id == 701 || id == 741) { //Fog
								path += "/5";
							} else if (id >= 800 && id < 803) { //Clear sky
								path += "/6";
							} else if (id >= 803) { //Cloudy
								path += "/7";
							}
						} else if (parser.getString().equals("temp") || parser.getString().equals("speed")) {
							parser.next();
							path += "/" + parser.getInt();
						}
					} 
				}
			}
		}
		return path;
	}
	
	public static void main(String[] args) {
		String PATH = getPath();
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(REST_URI).path(PATH);
        Response response = target.request().get();
        System.out.println(response.readEntity(String.class));
	}
}
